libpod (2.0.4+dfsg2-1) unstable; urgency=medium

  * Vendor in protobuf 3 to workaround #961814
  * Remove "insanity workaround" related to protobuf
  * Hand in forgotten changelog entry in 2.0.4+dfsg1-1

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 03 Aug 2020 07:20:45 -0400

libpod (2.0.4+dfsg1-1) unstable; urgency=medium

  * New upstream release
  * No longer install /etc/containers/libpod.conf (Closes: #961016)
      This file is deprecated in version 2.0 and is superseeded by
      /etc/containers/containers.conf, which is provided by the
      olang-github-containers-common package. The old file hardcodes
      a default OCI runtime that breaks in default installations.
  * Fixed REST API regression (Closes: #966501)

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 30 Jul 2020 07:12:41 -0400

libpod (2.0.3+dfsg1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Install systemd helper files in favor of varlink (Closes: #966118)

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 26 Jul 2020 10:53:39 -0400

libpod (2.0.2+dfsg1-3) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 20 Jul 2020 10:18:00 -0400

libpod (2.0.2+dfsg1-2) experimental; urgency=medium

  * Team upload.
  * debian/rules: Add XDG_RUNTIME_DIR settings on build
    - Based on debian/rules from the ibus package, unbreaks
      testsuite on many buildds

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 17 Jul 2020 06:56:20 -0400

libpod (2.0.2+dfsg1-1) experimental; urgency=medium

  * Team upload.
  * New upstream version, Closes: #964378

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 16 Jul 2020 18:06:15 -0400

libpod (1.6.4+dfsg1-4) unstable; urgency=medium

  * Team upload.
  * Rename golang-x-text-dev to golang-golang-x-text-dev

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 12 Jul 2020 18:51:51 +0800

libpod (1.6.4+dfsg1-3) unstable; urgency=high

  * Team upload.
  * Do not copy up when volume is not empty
    CVE-2020-1726, Closes: #961421

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 28 May 2020 17:24:41 -0400

libpod (1.6.4+dfsg1-2) unstable; urgency=medium

  * Un-vendored "golang-github-checkpoint-restore-go-criu-dev".
  * Tightened dependency: "conmon (>= 2.0.2~)".
  * rules:
    + Golang insanity workaround.
    + Removed obsolete "containers_image_ostree" build tag.

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 14 Jan 2020 10:56:58 +1100

libpod (1.6.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Install "seccomp.json".
  * Install tutorials.
  * Un-vendored "openshift/api" library.
  * Build-Depends:
    - golang-github-boltdb-bolt-dev
    + golang-github-coreos-bbolt-dev (>= 1.3.3~)
    - golang-github-containerd-continuity-dev
    = golang-github-containers-buildah-dev (>= 1.11.6~)
    = golang-github-containers-image-dev (>= 5.0.0~)
    + golang-github-openshift-api-dev

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 03 Jan 2020 08:36:51 +1100

libpod (1.6.2+dfsg-3) unstable; urgency=medium

  * Install annotated CNI examples.
  * Replaced default CNI "bridge" policy with "ptp".

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 31 Dec 2019 12:07:07 +1100

libpod (1.6.2+dfsg-2) unstable; urgency=medium

  * Added note about "swapaccount" to README.Debian.
  * libpod.conf: prefer "crun" over "runc".
  * Tightened "fuse-overlayfs" dependency.
  * Only install "registries.conf" example but not conf file.
  * Use "tini-static" for "init_path" built-in default instead of
    "catatonit".
  * Added "buildah" to Recommends since it provides "containers/image" man
    pages.
  * Standards-Version: 4.4.1

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 29 Dec 2019 20:49:01 +1100

libpod (1.6.2+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #930440).

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 12 Nov 2019 13:29:33 +1100
